import Rectangle.Rectangle;
import static Rectangle.Rectangle.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class RectangleTest {

    @Test
    void testAreaOfRectangleLength4AndBreadth8Gives32() {

        Rectangle rectangle = createRectangle(4.0, 8.0);

        assertEquals(32.0, rectangle.area());

    }

    @Test
    void testPerimeterOfRectangleLength4AndBreadth8Gives24() {

        Rectangle rectangle = createRectangle(4.0, 8.0);

        assertEquals(24.0, rectangle.perimeter());

    }

    @Test
    void testPerimeterOfRectangleLength12AndBreadth2Gives28() {

        Rectangle rectangle = createRectangle(12, 2);

        assertEquals(28.0, rectangle.perimeter());
    }

    @Test
    void testPerimeterOfRectangleLength5AndBreadth5Gives20() {

        Rectangle rectangle = createRectangle(5, 5);

        assertEquals(20.0, rectangle.perimeter());
    }

    @Test
    void testAreaOfSquareSide4Gives16() {

        Rectangle square = createSquare(4.0);

        assertEquals(16.0, square.area());

    }

    @Test
    void testPerimeterOfSquareSide20Gives80() {

        Rectangle square = createSquare(20);

        assertEquals(80.0, square.perimeter());

    }
}
